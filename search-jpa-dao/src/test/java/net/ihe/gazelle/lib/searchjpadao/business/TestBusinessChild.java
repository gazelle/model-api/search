package net.ihe.gazelle.lib.searchjpadao.business;

/**
 * Business object corresponding to {@link net.ihe.gazelle.lib.searchjpadao.adapter.TestSubEntity}
 */
public class TestBusinessChild {

    private String name;

    private String value;

    /**
     * Getter for the name property
     *
     * @return the value of the property
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the name property
     *
     * @param name the value to set to the property
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for the value property
     *
     * @return the value of the property
     */
    public String getValue() {
        return value;
    }

    /**
     * Setter for the value property
     *
     * @param value value to set to the property
     */
    public void setValue(String value) {
        this.value = value;
    }
}
