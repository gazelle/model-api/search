package net.ihe.gazelle.lib.searchjpadao.adapter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Test Entity for JPADAO testing with In-Memory Database.
 */
@Entity
@Table(name = "test_entity", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "test_entity_sequence", sequenceName = "test_entity_id_seq", allocationSize = 1)
public class TestEntity {

    @Id
    @GeneratedValue(generator = "test_entity_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "uuid")
    private String uuid;

    @OneToMany(mappedBy = "testEntity", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<TestSubEntity> testSubEntities = new ArrayList<>();

    /**
     * Default constructor for Hibernate to use.
     */
    public TestEntity() {
    }

    /**
     * Constructor for the class with an UUID
     *
     * @param uuid UUID for the entity
     */
    public TestEntity(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Getter for the uuid property
     *
     * @return the value of the UUID
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Getter for the testSubEntities property
     *
     * @return the value of the property.
     */
    public List<TestSubEntity> getTestSubEntities() {
        return testSubEntities;
    }

    /**
     * Setter for the testSubEntities property
     *
     * @param testSubEntities value to set to the property.
     */
    public void setTestSubEntities(List<TestSubEntity> testSubEntities) {
        this.testSubEntities = testSubEntities;
    }
}
