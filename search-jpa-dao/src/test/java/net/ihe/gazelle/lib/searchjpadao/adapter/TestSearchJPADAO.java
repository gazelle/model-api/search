package net.ihe.gazelle.lib.searchjpadao.adapter;

import net.ihe.gazelle.lib.searchjpadao.business.TestBusinessObject;

import javax.persistence.EntityManager;

/**
 * Test implementation of {@link SearchJPADAO}. Used during tests to search {@link TestEntity} in In-Memory Database.
 * Business class associated to the entity is {@link TestBusinessObject}.
 */
final class TestSearchJPADAO extends SearchJPADAO<TestBusinessObject, TestEntity> {

    /**
     * Constructor that matches super constructor with same signature.
     *
     * @param entityManager              : EntityManager used to create and execute requests.
     * @param criterionJPAMappingService : used to know where are located properties corresponding to each SearchCriterion.
     * @param resultJPAMappingService    : used to map properties between JPA and Business models.
     */
    TestSearchJPADAO(EntityManager entityManager, SearchCriterionJPAMappingService criterionJPAMappingService,
                     SearchResultJPAMappingService<TestBusinessObject, TestEntity> resultJPAMappingService) {
        super(entityManager, criterionJPAMappingService, resultJPAMappingService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<TestEntity> getJPAEntityClass() {
        return TestEntity.class;
    }
}
