package net.ihe.gazelle.lib.searchjpadao.adapter;

import net.ihe.gazelle.lib.searchjpadao.business.key.TestSearchCriterionKey;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.key.SearchCriterionKey;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

/**
 * Test implementation of {@link SearchCriterionJPAMappingService}
 */
public class TestSearchCriterionJPAMappingService implements SearchCriterionJPAMappingService {

    static final String CRITERION_KEY_CAST_ERROR = "Criterion Key cannot be casted to Test key.";

    private CriteriaBuilder criteriaBuilder;
    private EntityManager entityManager;

    /**
     * Private constructor to hide implicit one
     */
    private TestSearchCriterionJPAMappingService() {
    }

    /**
     * Default constructor for the class.
     *
     * @param entityManager used to get {@link Metamodel} and {@link CriteriaBuilder}
     */
    public TestSearchCriterionJPAMappingService(EntityManager entityManager) {
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
        this.entityManager = entityManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public From from(SearchCriterionKey key, From testEntityFrom) {
        TestSearchCriterionKey testKey = castToTestKey(key);
        Metamodel metamodel = entityManager.getMetamodel();
        EntityType<TestEntity> entityType = metamodel.entity(TestEntity.class);
        switch (testKey) {
            case UUID:
                return testEntityFrom;
            case SUB_ENTITY_VALUE:
            case SUB_ENTITY_NAME:
                From from = (From) testEntityFrom.getJoins().stream()
                        .filter(p ->
                                ((Join) p).getAttribute().getName().equals("testSubEntities") &&
                                        JoinType.LEFT.equals(((Join) p).getJoinType()))
                        .findAny().orElse(null);
                return from != null ? from : testEntityFrom.join(entityType.getList("testSubEntities", TestSubEntity.class), JoinType.LEFT);
            default:
                throw new IllegalArgumentException("Unsupported test key : " + testKey);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEntityProperty(SearchCriterionKey key) {
        TestSearchCriterionKey testKey = castToTestKey(key);
        switch (testKey) {
            case UUID:
                return "uuid";
            case SUB_ENTITY_NAME:
                return "name";
            case SUB_ENTITY_VALUE:
                return "value";
            default:
                throw new IllegalArgumentException("Unsupported test key : " + testKey);
        }
    }

    /**
     * Cast a {@link SearchCriterionKey} to {@link TestSearchCriterionKey}
     *
     * @param key the key to cast
     * @return the casted key
     */
    private TestSearchCriterionKey castToTestKey(SearchCriterionKey key) {
        try {
            return (TestSearchCriterionKey) key;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException(CRITERION_KEY_CAST_ERROR);
        }
    }
}
