package net.ihe.gazelle.lib.searchjpadao.business.key;

import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.key.SearchCriterionKey;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.operator.SearchCriterionOperator;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.operator.StringSearchCriterionOperator;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.option.SearchCriterionOptions;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.option.StringSearchCriterionOptions;

/**
 * Test {@link SearchCriterionKey} to perform searches on {@link net.ihe.gazelle.lib.searchjpadao.adapter.TestEntity}
 */
public enum TestSearchCriterionKey implements SearchCriterionKey {

    UUID(String.class, StringSearchCriterionOperator.EXACT, new StringSearchCriterionOptions(true)),
    SUB_ENTITY_NAME(String.class, StringSearchCriterionOperator.EXACT, new StringSearchCriterionOptions(true)),
    SUB_ENTITY_VALUE(String.class, StringSearchCriterionOperator.EXACT, new StringSearchCriterionOptions(true)),
    UNKNOWN_CRITERION(String.class, StringSearchCriterionOperator.EXACT, new StringSearchCriterionOptions(true)),
    UNKNOWN_OPERATOR(String.class, new SearchCriterionOperator() {
    }, new StringSearchCriterionOptions(true)),
    UNKNOWN_OPTIONS(String.class, StringSearchCriterionOperator.EXACT, new SearchCriterionOptions() {
    });


    private SearchCriterionOptions defaultOptions;
    private SearchCriterionOperator defaultOperator;
    private Class valueClass;

    /**
     * Default constructor for the class.
     *
     * @param clazz    class of the value defined by the criterion
     * @param operator operator to use for the criterion
     * @param options  options for the match using the criterion
     */
    TestSearchCriterionKey(Class clazz, SearchCriterionOperator operator, SearchCriterionOptions options) {
        this.valueClass = clazz;
        this.defaultOperator = operator;
        this.defaultOptions = options;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchCriterionOptions getDefaultOptions() {
        return this.defaultOptions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchCriterionOperator getDefaultOperator() {
        return this.defaultOperator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class getValueClass() {
        return this.valueClass;
    }
}
