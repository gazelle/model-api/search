package net.ihe.gazelle.lib.searchjpadao.adapter;

import net.ihe.gazelle.lib.searchjpadao.business.TestBusinessChild;
import net.ihe.gazelle.lib.searchjpadao.business.TestBusinessObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Test implementation of {@link SearchResultJPAMappingService}. Maps entity {@link TestEntity} to business object {@link TestBusinessObject}.
 */
public class TestSearchResultJPAMappingService implements SearchResultJPAMappingService<TestBusinessObject, TestEntity> {

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TestBusinessObject> mapToBusiness(List<TestEntity> jpaEntities) {
        List<TestBusinessObject> businessObjects = new ArrayList<>();
        for (TestEntity jpaEntity : jpaEntities){
            businessObjects.add(convertEntityToBusinessObject(jpaEntity));
        }
        return businessObjects;
    }

    /**
     * Actual conversion to business model.
     *
     * @param jpaEntity Entity to convert to business model
     * @return the converted {@link TestBusinessObject}
     */
    private TestBusinessObject convertEntityToBusinessObject(TestEntity jpaEntity){
        TestBusinessObject businessObject = new TestBusinessObject();
        businessObject.setUuid(jpaEntity.getUuid());
        for (TestSubEntity child : jpaEntity.getTestSubEntities()) {
            TestBusinessChild testBusinessChild = new TestBusinessChild();
            testBusinessChild.setName(child.getName());
            testBusinessChild.setValue(child.getValue());
            businessObject.getChildren().add(testBusinessChild);
        }
        return businessObject;
    }
}
