package net.ihe.gazelle.lib.searchjpadao.adapter;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.lib.searchjpadao.business.TestBusinessObject;
import net.ihe.gazelle.lib.searchjpadao.business.key.TestSearchCriterionKey;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteriaLogicalOperator;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.SearchCriterion;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.SearchCriterionFactory;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.StringSearchCriterion;
import org.junit.jupiter.api.*;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for {@link SearchJPADAO}
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SearchJPADAOTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";

    TestSearchJPADAO testDAO;
    EntityManager entityManager;

    /**
     * Initialization method to initialize test data in In-Memory database for all tests  in the class.
     */
    @BeforeAll
    public void initializeDatabase() {
        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        entityManager.getTransaction().begin();
        addTestEntitiesToDB(entityManager);
        entityManager.getTransaction().commit();
    }

    /**
     * Initialization method to create EntityManager and JPADAO test implementation.
     */
    @BeforeEach
    public void initializeEntityManager() {
        entityManager.getTransaction().begin();

        TestSearchCriterionJPAMappingService criterionJPAMappingService =
                new TestSearchCriterionJPAMappingService(entityManager);
        TestSearchResultJPAMappingService resultJPAMappingService = new TestSearchResultJPAMappingService();
        testDAO = new TestSearchJPADAO(entityManager, criterionJPAMappingService, resultJPAMappingService);
    }

    /**
     * Close the transaction corresponding to the test case when it is over.
     */
    @AfterEach
    public void commitTransaction() {
        entityManager.getTransaction().commit();
    }

    /**
     * Close the connection to the database after all tests are executed.
     */
    @AfterAll
    public void closeDatabase() {
        entityManager.close();
    }

    /**
     * Test SearchJPADAO Constructor with null EntityManager as parameter.
     * Expected to throw an IllegalArgumentException.
     */
    @Test
    void searchJPADAOConstructorWithNullEntityManager() {
        try {
            new TestSearchJPADAO(null, new TestSearchCriterionJPAMappingService(entityManager),
                    new TestSearchResultJPAMappingService());
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            assertEquals(SearchJPADAO.CONSTRUCTOR_ILLEGAL_ARGUMENT_ERROR, e.getMessage());
        }
    }

    /**
     * Test SearchJPADAO Constructor with null SearchCriterionJPAMappingService as parameter.
     * Expected to throw an IllegalArgumentException.
     */
    @Test
    void searchJPADAOConstructorWithNullSearchCriterionJPAMappingService() {
        try {
            new TestSearchJPADAO(entityManager, null,
                    new TestSearchResultJPAMappingService());
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            assertEquals(SearchJPADAO.CONSTRUCTOR_ILLEGAL_ARGUMENT_ERROR, e.getMessage());
        }
    }

    /**
     * Test SearchJPADAO Constructor with null SearchResultJPAMappingService as parameter.
     * Expected to throw an IllegalArgumentException.
     */
    @Test
    void searchJPADAOConstructorWithNullSearchResultJPAMappingService() {
        try {
            new TestSearchJPADAO(entityManager, new TestSearchCriterionJPAMappingService(entityManager),
                    null);
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            assertEquals(SearchJPADAO.CONSTRUCTOR_ILLEGAL_ARGUMENT_ERROR, e.getMessage());
        }
    }

    /**
     * Test SearchJPADAO Search with known SearchCriterion.
     * Expected to return corresponding Business Objects.
     */
    @Test
    @Covers(requirements = {"SEA-5", "SEA-8", "SEA-12"})
    void searchWithKnownCriterion() throws SearchException {
        String searchedUUID = "uuid1";

        SearchCriteria searchCriteria = new SearchCriteria();
        StringSearchCriterion searchCriterion =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.UUID);
        searchCriterion.setValue(searchedUUID);
        searchCriteria.addSearchCriterion(searchCriterion);

        List<TestBusinessObject> results = testDAO.search(searchCriteria);

        assertNotNull(results);
        assertEquals(1, results.size());
        TestBusinessObject uniqueResult = results.get(0);
        assertEquals(searchedUUID, uniqueResult.getUuid());
    }

    /**
     * Test SearchJPADAO Search with no SearchCriterion.
     * Expected to return all Business Objects.
     */
    @Test
    void searchWithNoCriterion() throws SearchException {
        SearchCriteria searchCriteria = new SearchCriteria();
        List<TestBusinessObject> results = testDAO.search(searchCriteria);

        assertNotNull(results);
        assertEquals(3, results.size());
    }

    /**
     * Test SearchJPADAO Search with no value in the SearchCriterion.
     * Expected to throw an IllegalArgumentException.
     */
    @Test
    void searchWithKnownCriterionButNoValue() {
        SearchCriteria searchCriteria = new SearchCriteria();
        StringSearchCriterion searchCriterion =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.UUID);
        searchCriterion.setKey(TestSearchCriterionKey.UUID);
        searchCriteria.addSearchCriterion(searchCriterion);
        SearchException searchException = assertThrows(SearchException.class, () -> testDAO.search(searchCriteria));
        assertEquals(SearchJPADAO.MISSING_CRITERION_VALUE_ERROR, searchException.getMessage());
    }

    /**
     * Test SearchJPADAO Search with Unknown SearchCriterion. The Key of the criterion is not known by the JPA implementation.
     * Expected to throw an IllegalArgumentException.
     */
    @Test
    void searchWithUnknownCriterion() {
        String searchedValue = "value";

        SearchCriteria searchCriteria = new SearchCriteria();
        StringSearchCriterion searchCriterion =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.UNKNOWN_CRITERION);
        searchCriterion.setValue(searchedValue);
        searchCriteria.addSearchCriterion(searchCriterion);
        SearchException searchException = assertThrows(SearchException.class, () -> testDAO.search(searchCriteria));
        assertEquals("Unsupported test key : " + TestSearchCriterionKey.UNKNOWN_CRITERION, searchException.getCause().getMessage());
    }

    /**
     * Test SearchJPADAO Search with Unknown SearchCriterion type.
     * Expected to throw an SearchServiceInternalException.
     */
    @Test
    void searchWithUnknownCriterionType() {
        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion searchCriterion = new SearchCriterion<Object>() {
            @Override
            public Object getValue() {
                return null;
            }

            @Override
            public void setValue(Object value) {

            }
        };
        searchCriterion.setKey(TestSearchCriterionKey.UNKNOWN_CRITERION);
        searchCriteria.addSearchCriterion(searchCriterion);
        SearchException searchException = assertThrows(SearchException.class, () -> testDAO.search(searchCriteria));
        assertEquals("Unsupported Criterion type for DAO queries : " + searchCriterion.getClass(), searchException.getMessage());
    }


    /**
     * Test SearchJPADAO Search with multiple known {@link SearchCriterion} linked with logical OR.
     * Expected to return corresponding Business Objects.
     */
    @Test
    @Covers(requirements = {"SEA-3", "SEA-30"})
    void searchWithMultipleKnownCriterionOR1() throws SearchException {
        String searchedName1 = "n4";
        String searchedName2 = "n3";

        SearchCriteria searchCriteria = new SearchCriteria();
        StringSearchCriterion searchCriterion1 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_NAME);
        searchCriterion1.setValue(searchedName1);
        StringSearchCriterion searchCriterion2 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_NAME);
        searchCriterion2.setValue(searchedName2);
        searchCriteria.addSearchCriterion(searchCriterion1);
        searchCriteria.addSearchCriterion(searchCriterion2);

        List<TestBusinessObject> results = testDAO.search(searchCriteria);

        assertNotNull(results);
        assertEquals(2, results.size(), "Result shall contain 2 entries");
        assertTrue(results.stream().anyMatch(item -> "uuid0".equals(item.getUuid())), "Result shall contain Patient with UUID : uuid0");
        assertTrue(results.stream().anyMatch(item -> "uuid1".equals(item.getUuid())), "Result shall contain Patient with UUID : uuid1");
    }

    /**
     * Test SearchJPADAO Search with multiple known {@link SearchCriterion} linked with logical OR.
     * Expected to return corresponding Business Objects.
     */
    @Test
    @Covers(requirements = {"SEA-3", "SEA-30"})
    void searchWithMultipleKnownCriterionOR2() throws SearchException {
        String searchedName1 = "n2";
        String searchedUuid = "uuid42";

        SearchCriteria searchCriteria = new SearchCriteria();
        StringSearchCriterion searchCriterion1 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_NAME);
        searchCriterion1.setValue(searchedName1);
        StringSearchCriterion searchCriterion2 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.UUID);
        searchCriterion2.setValue(searchedUuid);
        searchCriteria.addSearchCriterion(searchCriterion1);
        searchCriteria.addSearchCriterion(searchCriterion2);

        List<TestBusinessObject> results = testDAO.search(searchCriteria);

        assertNotNull(results);
        assertEquals(1, results.size(), "Result shall contain a unique entry.");
        assertTrue(results.stream().anyMatch(item -> "uuid0".equals(item.getUuid())), "Result shall contain Patient with UUID : uuid0");
    }

    /**
     * Test SearchJPADAO Search with multiple known {@link SearchCriterion} linked with logical OR.
     * Expected to return corresponding Business Objects.
     */
    @Test
    @Covers(requirements = {"SEA-3", "SEA-30"})
    void searchWithMultipleKnownCriterionOR3() throws SearchException {
        String searchedName = "n22";
        String searchedValue = "v5";

        SearchCriteria searchCriteria = new SearchCriteria();
        StringSearchCriterion searchCriterion1 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_NAME);
        searchCriterion1.setValue(searchedName);
        StringSearchCriterion searchCriterion2 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_VALUE);
        searchCriterion2.setValue(searchedValue);
        searchCriteria.addSearchCriterion(searchCriterion1);
        searchCriteria.addSearchCriterion(searchCriterion2);

        List<TestBusinessObject> results = testDAO.search(searchCriteria);

        assertNotNull(results);
        assertEquals(1, results.size(), "Result shall contain a unique entry.");
        assertTrue(results.stream().anyMatch(item -> "uuid1".equals(item.getUuid())), "Result shall contain Patient with UUID : uuid1");
    }

    /**
     * Test SearchJPADAO Search with multiple known {@link SearchCriterion} linked with logical OR.
     * Expected to return corresponding Business Objects.
     */
    @Test
    @Covers(requirements = {"SEA-3", "SEA-30"})
    void searchWithMultipleKnownCriterionORAndSearchCriteria() throws SearchException {
        String searchedName1 = "n4";
        String searchedName2 = "n3";

        SearchCriteria searchCriteria = new SearchCriteria();
        StringSearchCriterion searchCriterion1 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_NAME);
        searchCriterion1.setValue(searchedName1);

        SearchCriteria childSearchCriteria = new SearchCriteria();
        StringSearchCriterion searchCriterion2 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_NAME);
        searchCriterion2.setValue(searchedName2);
        searchCriteria.addSearchCriterion(searchCriterion1);
        childSearchCriteria.addSearchCriterion(searchCriterion2);
        searchCriteria.addSearchCriteria(childSearchCriteria);

        List<TestBusinessObject> results = testDAO.search(searchCriteria);

        assertNotNull(results);
        assertEquals(2, results.size(), "Result shall contain 2 entries");
        assertTrue(results.stream().anyMatch(item -> "uuid0".equals(item.getUuid())), "Result shall contain Patient with UUID : uuid0");
        assertTrue(results.stream().anyMatch(item -> "uuid1".equals(item.getUuid())), "Result shall contain Patient with UUID : uuid1");
    }

    /**
     * Test SearchJPADAO Search with multiple known {@link SearchCriterion} linked with logical AND.
     * Expected to return corresponding Business Objects.
     */
    @Test
    @Covers(requirements = {"SEA-3", "SEA-31"})
    void searchWithMultipleKnownCriterionAND() throws SearchException {
        String searchedName = "n1";
        String searchedUUID = "uuid0";

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.AND);
        StringSearchCriterion searchCriterion1 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_NAME);
        searchCriterion1.setValue(searchedName);
        StringSearchCriterion searchCriterion2 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.UUID);
        searchCriterion2.setValue(searchedUUID);
        searchCriteria.addSearchCriterion(searchCriterion1);
        searchCriteria.addSearchCriterion(searchCriterion2);

        List<TestBusinessObject> results = testDAO.search(searchCriteria);

        assertNotNull(results);
        assertEquals(1, results.size(), "Result shall contain a single entry");
        assertTrue(results.stream().anyMatch(item -> "uuid0".equals(item.getUuid())), "Result shall contain Patient with UUID : uuid0");
    }

    /**
     * Test SearchJPADAO Search with multiple known {@link SearchCriterion} linked with logical AND.
     * Expected to return corresponding Business Objects.
     */
    @Test
    @Covers(requirements = {"SEA-3", "SEA-31", "SEA-4"})
    void searchWithMultipleKnownCriterionANDOnList() throws SearchException {
        String searchedName1 = "n4";
        String searchedValue = "v4";

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.AND);
        StringSearchCriterion searchCriterion1 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_NAME);
        searchCriterion1.setValue(searchedName1);
        StringSearchCriterion searchCriterion2 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_VALUE);
        searchCriterion2.setValue(searchedValue);
        searchCriteria.addSearchCriterion(searchCriterion1);
        searchCriteria.addSearchCriterion(searchCriterion2);

        List<TestBusinessObject> results = testDAO.search(searchCriteria);

        assertNotNull(results);
        assertEquals(1, results.size(), "Result shall contain a unique entry.");
        assertTrue(results.stream().anyMatch(item -> "uuid1".equals(item.getUuid())), "Result shall contain Patient with UUID : uuid1");
    }

    /**
     * Test SearchJPADAO Search with multiple known {@link SearchCriterion} linked with logical AND.
     * Expected to return corresponding Business Objects.
     */
    @Test
    @Covers(requirements = {"SEA-3", "SEA-31"})
    void searchWithMultipleKnownCriterionANDNoResults() throws SearchException {
        String searchedUUID0 = "uuid0";
        String searchedUUID1 = "uuid1";

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.AND);
        StringSearchCriterion searchCriterion1 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.UUID);
        searchCriterion1.setValue(searchedUUID0);
        StringSearchCriterion searchCriterion2 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.UUID);
        searchCriterion2.setValue(searchedUUID1);
        searchCriteria.addSearchCriterion(searchCriterion1);
        searchCriteria.addSearchCriterion(searchCriterion2);

        List<TestBusinessObject> results = testDAO.search(searchCriteria);

        assertNotNull(results);
        assertEquals(0, results.size(), "Result shall contain no entry.");
    }

    /**
     * Test SearchJPADAO Search with multiple known {@link SearchCriterion} linked with logical AND.
     * Expected to return corresponding Business Objects.
     */
    @Test
    @Covers(requirements = {"SEA-3", "SEA-31", "SEA-4"})
    void searchWithMultipleKnownCriterionANDAndSearchCriteria() throws SearchException {
        String searchedUUID = "uuid1";
        String searchedName = "n4";
        String searchedValue = "v4";

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.AND);
        StringSearchCriterion searchCriterion1 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.UUID);
        searchCriterion1.setValue(searchedUUID);

        SearchCriteria childSearchCriteria = new SearchCriteria();
        childSearchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.AND);
        StringSearchCriterion searchCriterion2 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_NAME);
        searchCriterion2.setValue(searchedName);
        StringSearchCriterion searchCriterion3 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_VALUE);
        searchCriterion3.setValue(searchedValue);
        searchCriteria.addSearchCriterion(searchCriterion1);
        childSearchCriteria.addSearchCriterion(searchCriterion2);
        childSearchCriteria.addSearchCriterion(searchCriterion3);
        searchCriteria.addSearchCriteria(childSearchCriteria);

        List<TestBusinessObject> results = testDAO.search(searchCriteria);

        assertNotNull(results);
        assertEquals(1, results.size(), "Result shall contain a single entry");
        assertTrue(results.stream().anyMatch(item -> "uuid1".equals(item.getUuid())), "Result shall contain Patient with UUID : uuid1");
    }

    /**
     * Test SearchJPADAO Search with multiple known {@link SearchCriterion} linked with logical AND and OR.
     * Expected to return corresponding Business Objects.
     */
    @Test
    @Covers(requirements = {"SEA-3", "SEA-31", "SEA-30"})
    void searchWithMultipleKnownCriterionORANDSearchCriteria() throws SearchException {
        String searchedUUID = "uuid0";
        String searchedName = "n4";
        String searchedValue = "v4";

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.OR);
        StringSearchCriterion searchCriterion1 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.UUID);
        searchCriterion1.setValue(searchedUUID);

        SearchCriteria childSearchCriteria = new SearchCriteria();
        childSearchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.AND);
        StringSearchCriterion searchCriterion2 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_NAME);
        searchCriterion2.setValue(searchedName);
        StringSearchCriterion searchCriterion3 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_VALUE);
        searchCriterion3.setValue(searchedValue);
        searchCriteria.addSearchCriterion(searchCriterion1);
        childSearchCriteria.addSearchCriterion(searchCriterion2);
        childSearchCriteria.addSearchCriterion(searchCriterion3);
        searchCriteria.addSearchCriteria(childSearchCriteria);

        List<TestBusinessObject> results = testDAO.search(searchCriteria);

        assertNotNull(results);
        assertEquals(2, results.size(), "Result shall contain exactly two entries");
        assertTrue(results.stream().anyMatch(item -> "uuid0".equals(item.getUuid())), "Result shall contain Patient with UUID : uuid0");
        assertTrue(results.stream().anyMatch(item -> "uuid1".equals(item.getUuid())), "Result shall contain Patient with UUID : uuid1");
    }

    /**
     * Test SearchJPADAO Search with multiple known {@link SearchCriterion} linked with logical AND and OR.
     * Expected to return corresponding Business Objects.
     */
    @Test
    @Covers(requirements = {"SEA-3", "SEA-31", "SEA-30"})
    void searchWithMultipleKnownCriterionANDORSearchCriteria() throws SearchException {
        String searchedUUID = "uuid0";
        String searchedName = "n2";
        String searchedValue = "v4";

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.AND);
        StringSearchCriterion searchCriterion1 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.UUID);
        searchCriterion1.setValue(searchedUUID);

        SearchCriteria childSearchCriteria = new SearchCriteria();
        childSearchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.OR);
        StringSearchCriterion searchCriterion2 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_NAME);
        searchCriterion2.setValue(searchedName);
        StringSearchCriterion searchCriterion3 =
                (StringSearchCriterion) new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.SUB_ENTITY_VALUE);
        searchCriterion3.setValue(searchedValue);
        searchCriteria.addSearchCriterion(searchCriterion1);
        childSearchCriteria.addSearchCriterion(searchCriterion2);
        childSearchCriteria.addSearchCriterion(searchCriterion3);
        searchCriteria.addSearchCriteria(childSearchCriteria);

        List<TestBusinessObject> results = testDAO.search(searchCriteria);

        assertNotNull(results);
        assertEquals(1, results.size(), "Result shall contain exactly one entry");
        assertTrue(results.stream().anyMatch(item -> "uuid0".equals(item.getUuid())), "Result shall contain Patient with UUID : uuid0");
    }

    /**
     * Add entities in Database to test the search on.
     *
     * @param entityManager: EntityManager used to merge entities.
     */
    private void addTestEntitiesToDB(EntityManager entityManager) {
        for (TestEntity entity : getTestEntities()) {
            TestEntity mergedEntity = entityManager.merge(entity);
            for (TestSubEntity child : entity.getTestSubEntities()) {
                child.setTestEntity(mergedEntity);
                entityManager.merge(child);
            }
        }
    }

    /**
     * Getter for the list of existing entities for tests.
     *
     * @return the list of entities available for Search tests
     */
    private List<TestEntity> getTestEntities() {
        List<TestEntity> testEntities = new ArrayList<>();

        TestEntity testEntity0 = new TestEntity("uuid0");
        TestEntity testEntity1 = new TestEntity("uuid1");
        TestEntity testEntity2 = new TestEntity("uuid2");

        TestSubEntity testSubEntity00 = new TestSubEntity();
        testSubEntity00.setName("n1");
        testSubEntity00.setValue("v1");
        testSubEntity00.setTestEntity(testEntity0);
        TestSubEntity testSubEntity01 = new TestSubEntity();
        testSubEntity01.setName("n2");
        testSubEntity01.setValue("v1");
        testSubEntity01.setTestEntity(testEntity0);
        TestSubEntity testSubEntity02 = new TestSubEntity();
        testSubEntity02.setName("n3");
        testSubEntity02.setValue("v2");
        testSubEntity02.setTestEntity(testEntity0);

        testEntity0.getTestSubEntities().add(testSubEntity00);
        testEntity0.getTestSubEntities().add(testSubEntity01);
        testEntity0.getTestSubEntities().add(testSubEntity02);

        TestSubEntity testSubEntity10 = new TestSubEntity();
        testSubEntity10.setName("n4");
        testSubEntity10.setValue("v4");
        testSubEntity10.setTestEntity(testEntity1);
        TestSubEntity testSubEntity11 = new TestSubEntity();
        testSubEntity11.setName("n5");
        testSubEntity11.setValue("v4");
        testSubEntity11.setTestEntity(testEntity1);
        TestSubEntity testSubEntity12 = new TestSubEntity();
        testSubEntity12.setName("n6");
        testSubEntity12.setValue("v5");
        testSubEntity12.setTestEntity(testEntity1);

        testEntity1.getTestSubEntities().add(testSubEntity10);
        testEntity1.getTestSubEntities().add(testSubEntity11);
        testEntity1.getTestSubEntities().add(testSubEntity12);

        testEntities.add(testEntity0);
        testEntities.add(testEntity1);
        testEntities.add(testEntity2);
        return testEntities;
    }
}
