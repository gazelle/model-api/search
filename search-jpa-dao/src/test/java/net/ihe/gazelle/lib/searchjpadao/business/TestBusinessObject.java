package net.ihe.gazelle.lib.searchjpadao.business;

import java.util.ArrayList;
import java.util.List;

/**
 * Business object corresponding to test entities.
 */
public class TestBusinessObject {

    private String uuid;

    private List<TestBusinessChild> children = new ArrayList<>();

    /**
     * Getter for the uuid property
     *
     * @return the value of the property
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Setter for the property
     *
     * @param uuid value to set to the property
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Getter for the children property
     *
     * @return the value of the property
     */
    public List<TestBusinessChild> getChildren() {
        return children;
    }

    /**
     * Setter for the children property
     *
     * @param children value to set to the property
     */
    public void setChildren(List<TestBusinessChild> children) {
        this.children = children;
    }
}
