package net.ihe.gazelle.lib.searchjpadao.adapter;

import javax.persistence.*;

/**
 * Test Entity to reference in {@link TestEntity} to simulate complex models for test.
 */
@Entity
@Table(name = "test_sub_entity", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "test_sub_entity_sequence", sequenceName = "test_sub_entity_id_seq", allocationSize = 1)
public class TestSubEntity {

    @Id
    @GeneratedValue(generator = "test_sub_entity_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @ManyToOne(targetEntity = TestEntity.class)
    @JoinColumn(name = "test_entity_id")
    private TestEntity testEntity;

    @Column(name = "name")
    private String name;

    @Column(name = "value")
    private String value;

    /**
     * Default constructor for Hibernate to use.
     */
    public TestSubEntity() {
    }

    /**
     * Getter for the name property
     *
     * @return the value of the name property
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the name property
     *
     * @param name value to set to the property
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for the value property
     *
     * @return the value of the property
     */
    public String getValue() {
        return value;
    }

    /**
     * Setter for the value property
     *
     * @param value value to set to the property
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Getter for the testEntityProperty
     *
     * @return the value of the property
     */
    public TestEntity getTestEntity() {
        return testEntity;
    }

    /**
     * Setter for the testEntity property
     *
     * @param testEntity value to set to the property
     */
    public void setTestEntity(TestEntity testEntity) {
        this.testEntity = testEntity;
    }
}
