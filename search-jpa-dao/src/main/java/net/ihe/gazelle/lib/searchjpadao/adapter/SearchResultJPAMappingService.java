package net.ihe.gazelle.lib.searchjpadao.adapter;

import java.util.List;

/**
 * This interface defines the functions that can be used to map properties between JPA and Business models.
 *
 * @author wbars
 */
public interface SearchResultJPAMappingService<T, Z> {

    /**
     * This method convert all element in the given list from JPA to Business model.
     *
     * @param jpaEntities : List of JPA entities to convert.
     *
     * @return a {@link List}&lt;{@link T}&gt;, list of Business objects.
     */
    List<T> mapToBusiness(List<Z> jpaEntities);
}
