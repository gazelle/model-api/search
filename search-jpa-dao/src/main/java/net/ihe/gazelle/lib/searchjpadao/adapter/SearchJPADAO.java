package net.ihe.gazelle.lib.searchjpadao.adapter;

import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteriaLogicalOperator;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchService;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.MissingCriterionValueException;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchServiceInternalException;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.SearchCriterion;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.StringSearchCriterion;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.operator.StringSearchCriterionOperator;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@link SearchJPADAO} abstract class is a partial implementation of the {@link SearchService} API.
 * It contains basic JPA elements that allow to perform any search request defined with a {@link SearchCriteria} object.
 * It will use a {@link SearchCriterionJPAMappingService} implementation to know where to apply restriction,
 * and a {@link SearchResultJPAMappingService} implementation to map properties between the JPA and Business Model.
 *
 * @author wbars
 */
public abstract class SearchJPADAO<T, Z> implements SearchService<T> {

    static final String CONSTRUCTOR_ILLEGAL_ARGUMENT_ERROR = "Constructor arguments shall not be null !";
    static final String MISSING_CRITERION_VALUE_ERROR = "Search Criterion must all have a not null nor empty value.";

    private EntityManager entityManager;
    private CriteriaBuilder criteriaBuilder;
    private SearchCriterionJPAMappingService criterionJPAMappingService;
    private SearchResultJPAMappingService<T, Z> resultJPAMappingService;

    /**
     * This method build the {@link Predicate} object for a like restriction on the given property with given value.
     *
     * @param entityManager              : EntityManager used to create and execute requests.
     * @param criterionJPAMappingService : used to know where are located properties corresponding to each SearchCriterion.
     * @param resultJPAMappingService    : used to map properties between JPA and Business models.
     */
    public SearchJPADAO(EntityManager entityManager, SearchCriterionJPAMappingService criterionJPAMappingService,
                        SearchResultJPAMappingService<T, Z> resultJPAMappingService) {
        if (entityManager == null || criterionJPAMappingService == null || resultJPAMappingService == null) {
            throw new IllegalArgumentException(CONSTRUCTOR_ILLEGAL_ARGUMENT_ERROR);
        }
        this.entityManager = entityManager;
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
        this.criterionJPAMappingService = criterionJPAMappingService;
        this.resultJPAMappingService = resultJPAMappingService;
    }

    /**
     * This method is to be implemented for JPA to know which entity class is being retrieved by the request.
     *
     * @return a {@link Class}&lt;{@link Z}&gt; object.
     */
    protected abstract Class<Z> getJPAEntityClass();

    /**
     * This implementation of search {@link SearchService#search(SearchCriteria)} allows the caller to perform a
     * database query using JPA to retreive objects matching the search request defined by given {@link SearchCriteria}
     *
     * @param searchCriteria : Criteria defining the search request
     * @return a {@link List}&lt;{@link T}&gt;, list of Business object.
     * @throws SearchException when Illegal Arguments are passed to the method or if the request cannot be performed on Database.
     */
    @Override
    public List<T> search(SearchCriteria searchCriteria) throws SearchException {
        try {
            CriteriaQuery<Z> criteriaQuery = criteriaBuilder.createQuery(getJPAEntityClass());
            From entityDBRoot = criteriaQuery.from(getJPAEntityClass());

            Predicate predicate = getPredicateForSearchCriteria(searchCriteria, entityDBRoot);

            TypedQuery<Z> query;
            if (predicate != null) {
                query = entityManager.createQuery(criteriaQuery.select(entityDBRoot).where(predicate).distinct(true));
            } else {
                query = entityManager.createQuery(criteriaQuery.select(entityDBRoot).distinct(true));
            }
            return getBusinessResults(query);
        } catch (IllegalArgumentException e) {
            throw new SearchException("Illegal argument Criteria for Search request !", e);
        }
    }

    /**
     * Get results from the JPA Query and map them to the business model.
     *
     * @param query JPA Query
     * @return a list of business objects.
     * @throws SearchException if the query cannot be successfully performed on the Database.
     */
    private List<T> getBusinessResults(TypedQuery<Z> query) throws SearchException {
        try {
            List<Z> result = query.getResultList();
            return resultJPAMappingService.mapToBusiness(result);
        } catch (IllegalStateException | PersistenceException e) {
            throw new SearchServiceInternalException(e);
        }
    }

    /**
     * Get {@link Predicate} corresponding to a {@link SearchCriteria} for a database request.
     *
     * @param searchCriteria {@link SearchCriteria} defining the request
     * @return the value of the {@link Predicate} to use for the request, null if nothing is to filter.
     * @throws MissingCriterionValueException if the {@link SearchCriteria} contains a {@link SearchCriterion} that defines no value.
     * @throws SearchServiceInternalException if the {@link SearchCriteriaLogicalOperator} used is not supported or if the {@link SearchCriterion}
     *                                        defines a  {@link StringSearchCriterionOperator} or is of an unsupported type.
     */
    private Predicate getPredicateForSearchCriteria(SearchCriteria searchCriteria, From entityDBRoot) throws MissingCriterionValueException,
            SearchServiceInternalException {
        List<Predicate> searchCriteriaPredicates = new ArrayList<>();
        for (SearchCriteria childSearchCriteria : searchCriteria.getSearchCriterias()) {
            searchCriteriaPredicates.add(getPredicateForSearchCriteria(childSearchCriteria, entityDBRoot));
        }
        for (SearchCriterion childSearchCriterion : searchCriteria.getSearchCriterions()) {
            if (childSearchCriterion != null) {
                Predicate predicate = getPredicateForCriterion(entityDBRoot, childSearchCriterion);
                if (predicate == null) {
                    throw new MissingCriterionValueException(MISSING_CRITERION_VALUE_ERROR);
                } else {
                    searchCriteriaPredicates.add(predicate);
                }
            }
        }
        return aggregateSearchCriteriaPredicates(searchCriteria.getLogicalOperator(), searchCriteriaPredicates);
    }

    /**
     * Aggregate all Predicates for {@link SearchCriterion} contained in a {@link SearchCriteria} according to its
     * {@link SearchCriteriaLogicalOperator}.
     *
     * @param logicalOperator Logical Operator to use between predicates
     * @param predicates      list of {@link Predicate} to aggregate for the {@link SearchCriteria}
     * @return the aggregated {@link Predicate}
     * @throws SearchServiceInternalException if the {@link SearchCriteriaLogicalOperator} is not supported
     */
    private Predicate aggregateSearchCriteriaPredicates(SearchCriteriaLogicalOperator logicalOperator, List<Predicate> predicates) throws SearchServiceInternalException {
        if (predicates == null || predicates.isEmpty()) {
            return null;
        }
        if (SearchCriteriaLogicalOperator.OR.equals(logicalOperator)) {
            return criteriaBuilder.or(predicates.toArray(new Predicate[predicates.size()]));
        } else if (SearchCriteriaLogicalOperator.AND.equals(logicalOperator)) {
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        } else {
            throw new SearchServiceInternalException("Unsupported Search Criteria Logical Operator : " + logicalOperator);
        }
    }

    /**
     * This method return the JPA Predicate to use to filter according to a specific Criterion.
     *
     * @param entityDBRoot : {@link From} object where to find the property to filter on.
     * @param criterion    : {@link SearchCriterion} Criterion that define the filter to apply.
     * @return a {@link Predicate} corresponding to the criterion, null if no value is defined.
     */
    private Predicate getPredicateForCriterion(From entityDBRoot, SearchCriterion criterion) throws SearchServiceInternalException {
        if (criterion instanceof StringSearchCriterion) {
            return getPredicateForStringCriterion(criterionJPAMappingService.from(criterion.getKey(), entityDBRoot),
                    (StringSearchCriterion) criterion);
        } else {
            throw new SearchServiceInternalException("Unsupported Criterion type for DAO queries : " + criterion.getClass());
        }
    }

    /**
     * This method return the JPA Predicate to use to filter according to a specific String Criterion.
     *
     * @param entityDBRoot : {@link From} object where to find the property to filter on.
     * @param criterion    : {@link StringSearchCriterion} String Criterion that define the filter to apply.
     * @return a {@link Predicate}.
     */
    private Predicate getPredicateForStringCriterion(From entityDBRoot, StringSearchCriterion criterion) throws SearchServiceInternalException {
        String criterionValue = criterion.getValue();
        return getStringRestrictionIfValued(
                getStringPropertyExpression(entityDBRoot, criterionJPAMappingService.getEntityProperty(criterion.getKey()),
                        criterion.isCaseSensitive()),
                formatValue(criterionValue, criterion.getOperator()));
    }

    /**
     * This method build the {@link Predicate} object for a like restriction on the given property with given value.
     *
     * @param propertyExpression : JPA expression of the property
     * @param value              : value of the criterion
     * @return a {@link Predicate} object.
     */
    private Predicate getStringRestrictionIfValued(Expression<String> propertyExpression, String value) {
        if (value != null && !value.isEmpty()) {
            return criteriaBuilder.like(propertyExpression, value);
        }
        return null;
    }

    /**
     * This method build the {@link Expression<String>} object corresponding to the given property.
     *
     * @param queryRoot       : where the property can be found
     * @param property        : name of the JPA property
     * @param isCaseSensitive : whether or not the property is case sensitive or not
     * @return a {@link Predicate} object.
     */
    private Expression<String> getStringPropertyExpression(From queryRoot, String property, boolean isCaseSensitive) {
        Path<String> parameter = queryRoot.get(property);
        if (isCaseSensitive) {
            return criteriaBuilder.lower(parameter);
        } else {
            return parameter;
        }
    }

    /**
     * This method will format a string value to use it in a like JPA predicate to perform the operation defined by the
     * used {@link StringSearchCriterionOperator}.
     *
     * @param value    : value of the criterion
     * @param operator : operator used for the SearchCriterion restriction
     * @return a {@link String} object.
     * @throws SearchServiceInternalException when an illegal operator is used.
     */
    private String formatValue(final String value, StringSearchCriterionOperator operator) throws SearchServiceInternalException {
        if (operator == StringSearchCriterionOperator.EXACT) {
            return value;
        } else {
            throw new SearchServiceInternalException("Illegal operator for criterion value : " + operator.toString());
        }
    }
}
