package net.ihe.gazelle.lib.searchjpadao.adapter;

import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.key.SearchCriterionKey;

import javax.persistence.criteria.From;

/**
 * This interface defines the functions that can be used to know where the property associated to
 * a Criterion can be found by JPA and what it is named.
 *
 * @author wbars
 */
public interface SearchCriterionJPAMappingService {

    /**
     * Given a specific {@link SearchCriterionKey}, the class that implements this method shall return
     * the {@link From} object that correspond to the persisted property on which the SearchCriterion defines a restriction.
     * This can then be used by the caller to create a request for matching objects using JPA.
     *
     * @param key : Key of a specific criterion
     * @param entityDBRoot : Root of the request where joins can be applied if necessary
     * @return a {@link javax.persistence.criteria.From} object.
     */
    From from(SearchCriterionKey key, From entityDBRoot);

    /**
     * Given a specific {@link SearchCriterionKey}, the class that implements this method shall return
     * the {@link String} name of the property of the JPA entity on which the SearchCriterion defines a restriction.
     * This can then be used by the caller to create a request for matching objects using JPA.
     *
     * @param key : Key of a specific criterion
     *
     * @return a {@link String} object.
     */
    String getEntityProperty(SearchCriterionKey key);
}
