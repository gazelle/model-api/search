# Gazelle GITB Library

## Introduction

This project is a **library** that provides elements for using and implementing Search services in the Gazelle platform.
This allows tools to perform Search requests using a standard model of Criteria and also provide a generic implementation of a 
DAO using JPA to retrieve objects in Database using any combination of Criterion complying with the given model.

## How to use

### JPA DAO Implementation

#### Dependencies

To use the messaging client, add the following dependencies to your project `pom.xml` :

```xml
<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>lib.search-model-api</artifactId>
    <version>1.2.0</version>
</dependency>
<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>lib.search-jpa-dao</artifactId>
    <version>1.2.0</version>
</dependency>
```

First dependency correspond to the Criterion Model used by the Search library, allowing you to build your Criteria as Java classes in your project.    
The second dependency is the Generic JPA Implementation that can be used to perform Database Search on Any JPA Entity given Criteria objects from 
`search-model-api` module.

#### Criterion Keys

To use the `Search` library in your project, you first have to define the Criterion Keys that can be used to search on the business object to Search.
 
Here is a simple code example defining the Key to Search on the UUID of a business Patient : 

```java
/**
 * Patient Search Criterion Key to be used to perfom searches on Patient Demographics.
 */
public enum PatientSearchCriterionKey implements SearchCriterionKey {

    UUID (String.class, StringSearchCriterionOperator.EXACT, new StringSearchCriterionOptions(true));

    private SearchCriterionOptions defaultOptions;
    private SearchCriterionOperator defaultOperator;
    private Class valueClass;

    /**
     * Default constructor for the class.
     * @param clazz         class of the criterion's value
     * @param operator      operator to use by search engine to match this criterion
     * @param options
     */
    PatientSearchCriterionKey(Class clazz, SearchCriterionOperator operator, SearchCriterionOptions options){
        this.valueClass = clazz;
        this.defaultOperator = operator;
        this.defaultOptions = options;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchCriterionOptions getDefaultOptions() {
        return this.defaultOptions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchCriterionOperator getDefaultOperator() {
        return this.defaultOperator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class getValueClass() {
        return valueClass;
    }
}
``` 


Currently, only String criterion are supported by the library. 
To these criterion, you can only apply an `EXACT` match.
You can chose, using `StringSearchCriterionOptions` if the value is Case Sensitive or not.

#### Use Generic JPA DAO

There are three more classes to implement in order to use the generic implementation of JPA DAO :

* SearchCriterionJPAMappingService
* SearchResultJPAMappingService
* SearchJPADAO

##### SearchCriterionJPAMappingService

This interface, defined as follow, defines the methods needed by the Generic JPA DAO to locate properties from JPA Entities from `SearchCriteria` 
used for the request.

```java
/**
 * This interface defines the functions that can be used to know where the property associated to
 * a Criterion can be found by JPA and what it is named.
 *
 * @author wbars
 */
public interface SearchCriterionJPAMappingService {

    /**
     * Given a specific {@link SearchCriterionKey}, the class that implements this method shall return
     * the {@link From} object that correspond to the persisted property on which the SearchCriterion defines a restriction.
     * This can then be used by the caller to create a request for matching objects using JPA.
     *
     * @param key : Key of a specific criterion
     * @param entityDBRoot : Root of the request where joins can be applied if necessary
     * @return a {@link javax.persistence.criteria.From} object.
     */
    From from(SearchCriterionKey key, From entityDBRoot);

    /**
     * Given a specific {@link SearchCriterionKey}, the class that implements this method shall return
     * the {@link String} name of the property of the JPA entity on which the SearchCriterion defines a restriction.
     * This can then be used by the caller to create a request for matching objects using JPA.
     *
     * @param key : Key of a specific criterion
     *
     * @return a {@link String} object.
     */
    String getEntityProperty(SearchCriterionKey key);
}
```

##### SearchResultJPAMappingService

This service is used by the generic DAO to transform Entities retrieved by JPA from the Database to Business objects. 
Its interface is defined as follow : 

```java
/**
 * This interface defines the functions that can be used to map properties between JPA and Business models.
 *
 * @author wbars
 */
public interface SearchResultJPAMappingService<T, Z> {

    /**
     * This method convert all element in the given list from JPA to Business model.
     *
     * @param jpaEntities : List of JPA entities to convert.
     *
     * @return a {@link List}&lt;{@link T}&gt;, list of Business objects.
     */
    List<T> mapToBusiness(List<Z> jpaEntities);
}
```

##### SearchJPADAO

This class is abstract in the `Search` library and should be pretty easy to implement.
The only method you really need to develop is the `getJPAEntityClass` method that shall return the Java `Class` object corresponding to the JPA Entity.
You can also "override" the constructor for the class to restrict inputs and/or use injection, as in the following example using CDI : 

```java
/**
 * Concrete JPA DAO for a patient Search.
 */
@Named("searchDAOService")
public final class PatientSearchDAO extends SearchJPADAO<Patient, PatientDB> {

    /**
     * Default constructor for the class.
     * @param entityManager                 {@link EntityManager} to use to perform actions on Database
     * @param criterionJPAMappingService    {@link SearchCriterionJPAMappingService} mapping Patient Search Criteria to JPA path and properties
     * @param resultJPAMappingService       {@link SearchResultJPAMappingService} mapping JPA results to business objects
     */
    @Inject
    public PatientSearchDAO(@EntityManagerProducer.InjectEntityManager EntityManager entityManager,
                            SearchCriterionJPAMappingService criterionJPAMappingService,
                            SearchResultJPAMappingService<Patient,PatientDB> resultJPAMappingService) {
        super(entityManager, criterionJPAMappingService, resultJPAMappingService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<PatientDB> getJPAEntityClass(){
        return PatientDB.class;
    }
}
```