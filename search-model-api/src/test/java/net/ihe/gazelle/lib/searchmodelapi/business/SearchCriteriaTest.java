package net.ihe.gazelle.lib.searchmodelapi.business;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.SearchCriterion;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.SearchCriterionFactory;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.key.TestSearchCriterionKey;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test for {@link SearchCriteria} model.
 */
@Covers(requirements = {"SEA-3"})
public class SearchCriteriaTest {

    //TODO This test may change if we decide to use AND as default

    /**
     * Test the default value of the Logical Operator for any new {@link SearchCriteria} is
     * {@link SearchCriteriaLogicalOperator#OR}.
     */
    @Test
    public void testLogicalOperatorDefaultValue() {
        SearchCriteria searchCriteria = new SearchCriteria();

        assertEquals(SearchCriteriaLogicalOperator.OR, searchCriteria.getLogicalOperator(), "Default Value for Logical Operator shall be OR.");
    }

    /**
     * Test setter for {@link SearchCriteria} Logical Operator.
     */
    @Test
    public void testLogicalOperatorSetter() {
        SearchCriteria searchCriteria = new SearchCriteria();

        searchCriteria.setLogicalOperator(null);

        assertNull(searchCriteria.getLogicalOperator(), "Value should have been changed to null");
    }

    /**
     * Test the list of {@link SearchCriterion} in the {@link SearchCriteria} is by default set to an empty list.
     */
    @Test
    public void testSearchCriterionsIsInitialized() {
        SearchCriteria searchCriteria = new SearchCriteria();

        assertNotNull(searchCriteria.getSearchCriterions(), "SearchCriterions shall not be null.");
        assertEquals(0, searchCriteria.getSearchCriterions().size(), "SearchCriterions shall be empty");
    }

    /**
     * Test setter for {@link SearchCriterion} list of a {@link SearchCriteria}
     */
    @Test
    public void testSetSearchCriterions() {
        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion searchCriterion = new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.UUID);

        List<SearchCriterion> searchCriterions = new ArrayList<>();
        searchCriterions.add(searchCriterion);
        searchCriteria.setSearchCriterions(searchCriterions);

        assertEquals(1, searchCriteria.getSearchCriterions().size(), "SearchCriterions shall contain a single element.");
        assertEquals(searchCriterion, searchCriteria.getSearchCriterions().get(0), "SearchCriterions shall contain the added SearchCriterion");
    }

    /**
     * Test setter for {@link SearchCriterion} list of a {@link SearchCriteria} cannot set to null value but to an empty list instead..
     */
    @Test
    public void testCannotSetSearchCriterionsToNull() {
        SearchCriteria searchCriteria = new SearchCriteria();

        searchCriteria.setSearchCriterions(null);

        assertNotNull(searchCriteria.getSearchCriterions(), "SearchCriterions shall not be null.");
        assertEquals(0, searchCriteria.getSearchCriterions().size(), "List should be empty when setter is used with null.");
    }

    /**
     * Test we can add a {@link SearchCriterion} in the list of a {@link SearchCriteria}
     */
    @Test
    public void testAddSearchCriterion() {
        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion searchCriterion = new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.UUID);

        searchCriteria.addSearchCriterion(searchCriterion);

        assertEquals(1, searchCriteria.getSearchCriterions().size(), "SearchCriterions shall contain a single element.");
        assertEquals(searchCriterion, searchCriteria.getSearchCriterions().get(0), "SearchCriterions shall contain the added SearchCriterion");
    }

    /**
     * Test we cannot modify the {@link SearchCriterion} list of a {@link SearchCriteria} via the value returned by the getter.
     */
    @Test
    public void testCannotMutateGetSearchCriterion() {
        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion searchCriterion = new SearchCriterionFactory().createSearchCriterion(TestSearchCriterionKey.UUID);

        List<SearchCriterion> searchCriterions = searchCriteria.getSearchCriterions();
        searchCriterions.add(searchCriterion);

        assertEquals(0, searchCriteria.getSearchCriterions().size(), "SearchCriterions shall still contain no element.");
    }

    /**
     * Test the list of {@link SearchCriteria} in the {@link SearchCriteria} is by default set to an empty list.
     */
    @Test
    public void testSearchCriteriasIsInitialized() {
        SearchCriteria searchCriteria = new SearchCriteria();

        assertNotNull(searchCriteria.getSearchCriterias(), "SearchCriterias shall not be null.");
        assertEquals(0, searchCriteria.getSearchCriterias().size(), "SearchCriterias shall be empty");
    }

    /**
     * Test setter for {@link SearchCriteria} list of a {@link SearchCriteria}
     */
    @Test
    public void testSetSearchCriterias() {
        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriteria childSearchCriteria = new SearchCriteria();

        List<SearchCriteria> searchCriterias = new ArrayList<>();
        searchCriterias.add(childSearchCriteria);
        searchCriteria.setSearchCriterias(searchCriterias);

        assertEquals(1, searchCriteria.getSearchCriterias().size(), "SearchCriterias shall contain a single element.");
        assertEquals(childSearchCriteria, searchCriteria.getSearchCriterias().get(0), "SearchCriterias shall contain the added SearchCriteria");
    }

    /**
     * Test setter for {@link SearchCriteria} list of a {@link SearchCriteria} cannot set to null value but to an empty list instead..
     */
    @Test
    public void testCannotSetSearchCriteriasToNull() {
        SearchCriteria searchCriteria = new SearchCriteria();

        searchCriteria.setSearchCriterias(null);

        assertNotNull(searchCriteria.getSearchCriterias(), "SearchCriterions shall not be null.");
        assertEquals(0, searchCriteria.getSearchCriterias().size(), "List should be empty when setter is used with null.");
    }


    /**
     * Test we can add a {@link SearchCriteria} in the list of a {@link SearchCriteria}
     */
    @Test
    public void testAddSearchCriteria() {
        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriteria childSearchCriteria = new SearchCriteria();

        searchCriteria.addSearchCriteria(childSearchCriteria);

        assertEquals(1, searchCriteria.getSearchCriterias().size(), "SearchCriterias shall contain a single element.");
        assertEquals(childSearchCriteria, searchCriteria.getSearchCriterias().get(0), "SearchCriterias shall contain the added SearchCriteria");
    }

    /**
     * Test we cannot modify the {@link SearchCriteria} list of a {@link SearchCriteria} via the value returned by the getter.
     */
    @Test
    public void testCannotMutateGetSearchCriteria() {
        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriteria childSearchCriteria = new SearchCriteria();

        List<SearchCriteria> searchCriterias = searchCriteria.getSearchCriterias();
        searchCriterias.add(childSearchCriteria);

        assertEquals(0, searchCriteria.getSearchCriterias().size(), "SearchCriterias shall still contain no element.");
    }
}
