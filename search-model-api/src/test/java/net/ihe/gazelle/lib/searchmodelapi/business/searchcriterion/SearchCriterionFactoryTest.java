package net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.key.TestSearchCriterionKey;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.operator.StringSearchCriterionOperator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link SearchCriterionFactory} class.
 */
public class SearchCriterionFactoryTest {

    private final static String STRING_TEST_VALUE = "TEST";

    /**
     * Test SearchCriterionFactory for an OK StringCriterionKey.
     */
    @Test
    @Covers(requirements = {"SEA-5","SEA-8","SEA-12","SEA-15"})
    public void TestSearchCriterionFactoryForStringCriterionKeyOK(){
        SearchCriterionFactory factory = new SearchCriterionFactory();
        SearchCriterion criterion = factory.createSearchCriterion(TestSearchCriterionKey.UUID);
        assertEquals(StringSearchCriterion.class, criterion.getClass());
        assertEquals(TestSearchCriterionKey.UUID, criterion.getKey());
        assertEquals(StringSearchCriterionOperator.EXACT, ((StringSearchCriterion) criterion).getOperator());
        assertTrue(((StringSearchCriterion) criterion).isCaseSensitive());
        assertNull(criterion.getValue());
        criterion.setValue(STRING_TEST_VALUE);
        assertEquals(STRING_TEST_VALUE, criterion.getValue());
    }

    /**
     * Test SearchCriterionFactory for an StringCriterionKey with an unknown Operator.
     */
    @Test
    public void TestSearchCriterionFactoryForStringCriterionUnknownOperator(){
        SearchCriterionFactory factory = new SearchCriterionFactory();
        try {
            SearchCriterion criterion = factory.createSearchCriterion(TestSearchCriterionKey.UNKNOWN_OPERATOR);
            fail("Expected IllegalArgumentException.");
        } catch (IllegalArgumentException e){
            assertTrue(e.getMessage().contains("Unexpected operator type for String Criterion"));
        }
    }

    /**
     * Test SearchCriterionFactory for an StringCriterionKey with unknown Options.
     */
    @Test
    public void TestSearchCriterionFactoryForStringCriterionUnknownOptions(){
        SearchCriterionFactory factory = new SearchCriterionFactory();
        try {
            SearchCriterion criterion = factory.createSearchCriterion(TestSearchCriterionKey.UNKNOWN_OPTIONS);
            fail("Expected IllegalArgumentException.");
        } catch (IllegalArgumentException e){
            assertTrue(e.getMessage().contains("Unexpected options type for String Criterion"));
        }
    }
}
