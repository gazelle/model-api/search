package net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.option;

/**
 * This class allows to define all options that can be used on String Criterion.
 * This includes :
 * <ul>
 *      <li>isCaseSensitive : the String value shall be match is a case sensitive manner.</li>
 * </ul>
 *
 * @author wbars
 */
public class StringSearchCriterionOptions implements SearchCriterionOptions{

    private boolean isCaseSensitive;

    /**
     * Private default constructor. Shall not be used so we do have complete option set.
     */
    private StringSearchCriterionOptions(){
    }

    /**
     * Public constructor. Shall be used so we do have complete option set.
     *
     * @param isCaseSensitive: value for the isCaseSensitive option.
     */
    public StringSearchCriterionOptions(boolean isCaseSensitive){
        this.isCaseSensitive = isCaseSensitive;
    }

    /**
     * Getter for the isCaseSensitive property.
     *
     * @return : isCaseSensitive value.
     */
    public boolean isCaseSensitive(){
        return isCaseSensitive;
    }
}
