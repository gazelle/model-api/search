package net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.operator;

/**
 * This interface is used to abstract Operators on SearchCriterion.
 *
 * @author wbars
 */
public interface SearchCriterionOperator {
}
