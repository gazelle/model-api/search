package net.ihe.gazelle.lib.searchmodelapi.business;

/**
 * This enumeration defines all Logical Operator that can be applied between Criterion of a same search.
 * <ul>
 * <li>OR : all Criterion are linked with a logical OR</li>
 * <li>AND : all Criterion are linked with a logical AND</li>
 * </ul>
 *
 * @author wbars
 */
public enum SearchCriteriaLogicalOperator {
    OR,
    AND
}
