package net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.key;

import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.operator.SearchCriterionOperator;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.option.SearchCriterionOptions;

/**
 * This interface defines the functions that a Criterion's Key defines. This will allow the caller to get information on the
 * default behavior of the Criterion.
 *
 * @author wbars
 */
public interface SearchCriterionKey {

    /**
     * Getter for the defaultOptions property.
     * @return Value of the defaultOptions property
     */
    SearchCriterionOptions getDefaultOptions();

    /**
     * Getter for the defaultOperator property.
     * @return Value of the defaultOperator property
     */
    SearchCriterionOperator getDefaultOperator();

    /**
     * Getter for the valueClass property.
     * @return Value of the valueClass property
     */
    Class getValueClass();
}
