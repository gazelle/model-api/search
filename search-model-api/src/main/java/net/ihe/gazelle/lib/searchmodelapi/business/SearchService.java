package net.ihe.gazelle.lib.searchmodelapi.business;

import net.ihe.gazelle.lib.searchmodelapi.business.exception.MissingCriterionValueException;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchServiceInternalException;

import java.util.List;

/**
 * This interface defines the function used for a Search Request.
 *
 * @author wbars
 */
public interface SearchService<T> {
    
     /**
      * Search methods to retrieve a {@link T}.
      *
      * @param searchCriteria : {@link SearchCriteria} object defining the Search Request.
      *
      * @return {@link List}&lt;{@link T}&gt;, list of object matching the Request Criteria, or an empty list if there is no match.
      *
      * @throws MissingCriterionValueException when a criterion defines no value to filter on.
      * @throws SearchServiceInternalException on unexpected exception encountered by the Search Service implementation.
     */
    List<T> search(SearchCriteria searchCriteria) throws SearchException;
}
