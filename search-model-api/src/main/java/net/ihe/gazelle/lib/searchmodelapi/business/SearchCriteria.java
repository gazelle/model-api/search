package net.ihe.gazelle.lib.searchmodelapi.business;

import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.SearchCriterion;

import java.util.ArrayList;
import java.util.List;

/**
 * This class holds the structure that defines a Search request.
 *
 * @author wbars
 */
public class SearchCriteria {

    private SearchCriteriaLogicalOperator logicalOperator = SearchCriteriaLogicalOperator.OR;

    private List<SearchCriteria> searchCriterias = new ArrayList<>();
    private List<SearchCriterion> searchCriterions = new ArrayList<>();

    /**
     * Getter for the logicalOperator property
     *
     * @return Value of the logicalOperator property
     */
    public SearchCriteriaLogicalOperator getLogicalOperator() {
        return logicalOperator;
    }

    /**
     * Setter for the logicalOperator property
     *
     * @param logicalOperator : logical operator to use between criterion
     */
    public void setLogicalOperator(SearchCriteriaLogicalOperator logicalOperator) {
        this.logicalOperator = logicalOperator;
    }

    /**
     * Getter for the searchCriterions property.
     *
     * @return Value of the searchCriterions property
     */
    public List<SearchCriterion> getSearchCriterions() {
        return new ArrayList<>(this.searchCriterions);
    }

    /**
     * Setter for the searchCriterions property
     *
     * @param searchCriterions : search criterion list to use for the request.
     */
    public void setSearchCriterions(List<SearchCriterion> searchCriterions) {
        if (searchCriterions != null) {
            this.searchCriterions = new ArrayList<>(searchCriterions);
        } else {
            this.searchCriterions = new ArrayList<>();
        }
    }

    /**
     * Add a {@link SearchCriterion} to the request.
     *
     * @param searchCriterion : Search Criterion to add.
     */
    public void addSearchCriterion(SearchCriterion searchCriterion) {
        this.searchCriterions.add(searchCriterion);
    }

    /**
     * Getter for the searchCriterias property.
     *
     * @return Value of the searchCriterias property
     */
    public List<SearchCriteria> getSearchCriterias() {
        return new ArrayList<>(this.searchCriterias);
    }

    /**
     * Setter for the searchCriterias propert
     *
     * @param searchCriterias : search criteria list to use for the request.
     */
    public void setSearchCriterias(List<SearchCriteria> searchCriterias) {
        if (searchCriterias != null) {
            this.searchCriterias = new ArrayList<>(searchCriterias);
        } else {
            this.searchCriterias = new ArrayList<>();
        }
    }

    /**
     * Add a {@link SearchCriteria} to the request.
     *
     * @param searchCriteria : Search Criteria to add.
     */
    public void addSearchCriteria(SearchCriteria searchCriteria) {
        this.searchCriterias.add(searchCriteria);
    }
}
