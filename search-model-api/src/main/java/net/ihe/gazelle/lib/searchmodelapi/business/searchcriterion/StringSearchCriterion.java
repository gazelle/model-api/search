package net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion;

import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.key.SearchCriterionKey;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.operator.StringSearchCriterionOperator;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.option.StringSearchCriterionOptions;

/**
 * This class holds the structure that defines a single String Criterion from a Search request.
 *
 * @author wbars
 */
public class StringSearchCriterion extends SearchCriterion<String> {

    private StringSearchCriterionOptions options;
    private StringSearchCriterionOperator operator;
    private String value;

    /**
     * Private default constructor. Shall not be used so we do have complete option set.
     */
    private StringSearchCriterion(){
    }

    /**
     * Package private constructor. Shall be used only via the factory.
     *
     * @param key: key of the created Criterion.
     */
    public StringSearchCriterion(SearchCriterionKey key){
        if(!(key.getDefaultOptions() instanceof StringSearchCriterionOptions)){
            throw new IllegalArgumentException("Unexpected options type for String Criterion : " + key.getDefaultOptions().getClass());
        } else if (!(key.getDefaultOperator() instanceof StringSearchCriterionOperator)){
            throw new IllegalArgumentException("Unexpected operator type for String Criterion : " + key.getDefaultOperator().getClass());
        }
        setKey(key);
        this.options = (StringSearchCriterionOptions) key.getDefaultOptions();
        this.operator = (StringSearchCriterionOperator) key.getDefaultOperator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getValue() {
        return this.value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Getter for the operator property.
     * @return Value of the operator property
     */
    public StringSearchCriterionOperator getOperator(){
        return this.operator;
    }

    /**
     * Getter for the isCaseSensitive property.
     * @return Value of the isCaseSensitive property
     */
    public boolean isCaseSensitive(){
        return this.options.isCaseSensitive();
    }
}
