package net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.operator;

/**
 * This enumeration defines all operator that can be applied to a String Criterion.
 * <ul>
 *     <li>EXACT : match the exact value of the Criterion.</li>
 * </ul>
 *
 * @author wbars
 */
public enum StringSearchCriterionOperator implements SearchCriterionOperator {
    EXACT
}
