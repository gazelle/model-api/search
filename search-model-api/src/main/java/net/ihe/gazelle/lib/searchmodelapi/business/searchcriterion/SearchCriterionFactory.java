package net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion;

import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.key.SearchCriterionKey;

/**
 * This class generates SearchCriterion object from a specific key. It allows to create the right kind of criterion,
 * holding the correct type of value for a specific property.
 *
 * @author wbars
 */
public class SearchCriterionFactory {

    /**
     * Creates an instance of SearchCriterion that will
     *
     * @param searchCriterionKey: the key will be used to generate the right kind of criterion. It will also set default behavior for the criterion.
     *
     * @return Value of the Value property
     */
    public SearchCriterion createSearchCriterion(SearchCriterionKey searchCriterionKey){
        Class criterionValueClass = searchCriterionKey.getValueClass();
        if (criterionValueClass == String.class){
            return new StringSearchCriterion(searchCriterionKey);
        } else {
            throw new UnsupportedOperationException("No criterion implementation existing for value class : " + criterionValueClass);
        }
    }
}
