package net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion;

import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.key.SearchCriterionKey;

/**
 * This class holds the structure that defines a single Criterion from a Search request.
 * This basically correspond to a single restriction on one of the target object properties.
 *
 * @author wbars
 */
public abstract class SearchCriterion<T> {

    private SearchCriterionKey key;

    /**
     * Getter for the value property.
     * @return Value of the Value property
     */
    public abstract T getValue();

    /**
     * Setter for the value property
     * @param value : value to restrict on.
     */
    public abstract void setValue(T value);

    /**
     * Getter for the key property.
     * @return Value of the key property
     */
    public SearchCriterionKey getKey() {
        return key;
    }

    /**
     * Setter for the key property
     * @param key : key of the Criterion
     */
    public void setKey(SearchCriterionKey key) {
        this.key = key;
    }
}
